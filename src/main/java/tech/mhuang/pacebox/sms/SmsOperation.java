package tech.mhuang.pacebox.sms;

import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.sms.domain.SmsSendResult;
import tech.mhuang.pacebox.sms.inteceptor.SmsSendInterceptor;

/**
 * 短信操作接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface SmsOperation {

    /**
     * 添加短信处理器
     *
     * @param key        短信处理的key
     * @param smsHandler 短信处理类
     */
    void addHandler(String key, BaseSmsHandler smsHandler);

    /**
     * 添加短信发送拦截器
     *
     * @param sendInterceptor 短信发送拦截器
     */
    void addInteceptor(SmsSendInterceptor sendInterceptor);

    /**
     * 短信发送
     *
     * @param smsSendRequest 发送的短信内容
     * @return 短信返回结果
     */
    SmsSendResult send(SmsSendRequest smsSendRequest);
}