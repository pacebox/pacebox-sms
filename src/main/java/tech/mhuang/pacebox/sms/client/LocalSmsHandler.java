package tech.mhuang.pacebox.sms.client;

import tech.mhuang.pacebox.sms.BaseSmsHandler;
import tech.mhuang.pacebox.sms.SmsFieldProperties;
import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.sms.domain.SmsSendResult;

/**
 * 本地短信发送（用户模拟调用）
 *
 * @author mhuang
 * @since 1.0.0
 */
public class LocalSmsHandler implements BaseSmsHandler {
    private SmsFieldProperties properties;

    @Override
    public void setFieldProperties(SmsFieldProperties properties) {
        this.properties = properties;
    }

    @Override
    public SmsSendResult send(SmsSendRequest smsSendRequest) {
        return SmsSendResult.builder().success(true).extendParam(smsSendRequest.getExtendParam()).build();
    }
}
