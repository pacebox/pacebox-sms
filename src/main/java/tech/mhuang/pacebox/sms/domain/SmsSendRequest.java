package tech.mhuang.pacebox.sms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.core.dict.BasicDict;

/**
 * 短信发送工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SmsSendRequest {

    /**
     * 冗余短信key
     */
    String type;

    /**
     * 调用短信得应用id
     */
    String appId;
    /**
     * 手机号
     */
    String mobile;

    /**
     * 消息内容
     */
    String content;

    /**
     * 签名
     */
    String sign;

    /**
     * 替换的模板code
     */
    String templateCode;

    /**
     * 模板参数
     */
    BasicDict templateParam;

    /**
     * 扩展参数、用于自行业务处理(采用回调时会带入)
     */
    BasicDict extendParam;

    /**
     * 回调url。云片短信时存在
     */
    String callBackUrl;
}