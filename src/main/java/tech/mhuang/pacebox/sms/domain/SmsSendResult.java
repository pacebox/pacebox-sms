package tech.mhuang.pacebox.sms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.core.dict.BasicDict;

/**
 * 短信发送的应答类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SmsSendResult {

    /**
     * 回执ID
     */
    private String bizId;

    /**
     * 请求id
     */
    private String requestId;

    /**
     * 短信真实状态
     */
    private boolean success;

    /**
     * 短信反馈本文
     */
    private String message;

    /**
     * 扩展参数、用于自行业务处理
     */
    private BasicDict extendParam;

    /**
     * 异常
     */
    private Throwable throwable;
}