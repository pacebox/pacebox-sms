package tech.mhuang.pacebox.sms;

import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.sms.domain.SmsSendResult;

/**
 * 短信接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseSmsHandler {

    /**
     * 设置属性
     *
     * @param properties 属性
     */
    void setFieldProperties(SmsFieldProperties properties);

    /**
     * 短信发送
     *
     * @param smsSendRequest 短信发送实体
     * @return 短信发送结果
     */
    SmsSendResult send(SmsSendRequest smsSendRequest);
}