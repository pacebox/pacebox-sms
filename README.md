<p align="center">
pacebox-sms 基于pacebox & 第三方短信 融合的工具包
</p>
<p align="center">
-- 主页：<a href="http://mhuang.tech/pacebox-sms">http://mhuang.tech/pacebox-sms</a>  --
</p>
<p align="center">
    -- QQ群①:<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=6703688b236038908f6c89b732758d00104b336a3a97bb511048d6fdc674ca01"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="pacebox官方交流群①" title="pacebox官方交流群①"></a>
</p>
---------------------------------------------------------------------------------------------------------------------------------------------------------

## 简介
pacebox-sms 是一个基于pacebox & 第三方短信（BAT）融合的工具包、屏蔽第三方调用相关接口进行统一化封装
## 结构
```
    - domain     封装的请求、响应包
    - extract    第三方扩展工具包
        - aliyun     阿里云短信封装
        - bdyun      百度云短信封装
        - tencent    腾讯云短信封装
    - inteceptor         通用拦截器
    - BaseSmsHandler     短信接口
    - SmsFieldProperties 短信配置属性类
    - SmsOperation       短信操作接口
    - SmsTemplate        短信模板调用类
```

## 安装

### MAVEN
在pom.xml中加入
```
    <dependency>
        <groupId>tech.mhuang.pacebox</groupId>
        <artifactId>pacebox-sms</artifactId>
        <version>${laster.version}</version>
    </dependency>
```
### 非MAVEN
下载任意链接
- [Maven中央库1](https://repo1.maven.org/maven2/tech/mhuang/pacebox/pacebox-sms/)
- [Maven中央库2](http://repo2.maven.org/maven2/tech/mhuang/pacebox/pacebox-sms/)

## demo案例

### SpringBoot
[点击访问源码](http://gitee.com/pacebox/inter-boot-demo)

> 注意
> pacebox只支持jdk1.8以上的版本
